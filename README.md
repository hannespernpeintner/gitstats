# GitStats#

A small GUI tool, that analyzes a given GIT repository. For example it lists all commits, gives you bar charts over commits by author and commits by day time and states what the most used words in all commit messages are.

![gitstats_overview.PNG](https://bitbucket.org/repo/4yAnrj/images/2904335276-gitstats_overview.PNG)