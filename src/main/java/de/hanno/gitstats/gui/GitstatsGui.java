package de.hanno.gitstats.gui;

import com.alee.extended.filechooser.WebDirectoryChooser;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebList;
import com.alee.laf.list.WebListModel;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tabbedpane.WebTabbedPane;
import com.alee.laf.table.WebTable;
import com.alee.managers.notification.NotificationIcon;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotificationPopup;
import com.alee.utils.swing.DialogOptions;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.time.TimeSeries;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class GitstatsGui {


    private static JFrame frame;
    private WebDirectoryChooser chooser;
    Repository localRepo = null;
    Git git = null;

    private Iterable<RevCommit> commits;

    private WebTabbedPane tabbedPane;

    private WebList commitList;
    private WebScrollPane commitListPane;

    private ChartPanel commitsByAuthorPanel;
    private JFreeChart commitsyAuthorChart;
    private DefaultPieDataset commitsByAuthorDataset;

    private ChartPanel commitsByTimePanel;
    private JFreeChart commitsyTimeChart;
    private DefaultCategoryDataset comitsByTimeDataset;

    private WebScrollPane wordCountPane;
    private WebTable wordCountTable;
    Map<String, Integer> wordCount = new HashMap<>();

    public GitstatsGui() {
        this(1024, 768);
    }

    public GitstatsGui(int width, int height) {
        frame = new JFrame("HPGitstats");
        frame.setSize(width, height);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        WebMenuBar menuBar = new WebMenuBar ();
        WebMenuItem openDirectoryMenuItem = new WebMenuItem("Open Directory");
        openDirectoryMenuItem.addActionListener(e -> {
            showOpenDirectoryDialog();
        });
        menuBar.add(openDirectoryMenuItem);

        chooser = new WebDirectoryChooser(frame, "Choose git directory" );
        chooser.setSelectedDirectory(new java.io.File("."));

        commitList = new WebList(new WebListModel<>());
        commitList.setVisibleRowCount(200);
        commitList.setSelectedIndex(0);
        commitList.setPreferredSize(new Dimension(1000, 1000));

        commitListPane = new WebScrollPane(new GroupPanel(commitList));

        commitsByAuthorDataset = new DefaultPieDataset();
        commitsyAuthorChart = ChartFactory.createPieChart(
                "Commits by author",  // chart title
                commitsByAuthorDataset,        // data
                true,           // include legend
                true,
                false);

        commitsByAuthorPanel = new ChartPanel(commitsyAuthorChart);
        commitsByAuthorPanel.setPreferredSize(new Dimension(10000,10000));


        comitsByTimeDataset = new DefaultCategoryDataset();
        commitsyTimeChart = ChartFactory.createBarChart(
                "Commits by time",
                "Users",
                "Commits",
                comitsByTimeDataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        commitsByTimePanel = new ChartPanel(commitsyTimeChart);

        String[] headers = {"Word", "Count"};
        String[][] data = new String[wordCount.entrySet().size()][];

        wordCountTable = new WebTable ( data, headers );
        wordCountTable.setEditable ( false );
        wordCountPane = new WebScrollPane(wordCountTable);

        tabbedPane = new WebTabbedPane();
        tabbedPane.setTabPlacement(WebTabbedPane.TOP);
        tabbedPane.addTab("All commits", commitListPane);
        tabbedPane.addTab("Commits by author", commitsByAuthorPanel);
        tabbedPane.addTab("Commits by time", commitsByTimePanel);
        tabbedPane.addTab("Most used words in commits", wordCountPane);

        frame.setJMenuBar(menuBar);
        frame.getContentPane().add(tabbedPane);
        frame.setVisible(true);
    }

    public void showOpenDirectoryDialog() {
        chooser.setVisible(true);

        String localPath = "";
        if (chooser.getResult() == DialogOptions.OK_OPTION) {
            localPath = chooser.getSelectedDirectory().toPath().toString();
        }
        else {
            showError("No Selection");
        }
        try {
            localRepo = new FileRepository(localPath + "/.git");
            git = new Git(localRepo);
            

        } catch (IOException e) {
            showError(e.getMessage());
        } finally {
            try {
                init();
                showSuccess("Initialization finished");
            } catch (IOException | GitAPIException e) {
                showError("No initialization");
            }
        }
    }

    private void init() throws IOException, GitAPIException {
        commitList.getWebModel().clear();
        commitsByAuthorPanel.removeAll();

        if(git == null || localRepo == null) {
            showError("No repo present");
            return;
        }

        LogCommand all = git.log().all();
        commits = all.call();

        Map<PersonIdent, Integer> commitsByAuthor = new HashMap<>();

        wordCount.clear();

        for (RevCommit commit : commits) {
            String name = commit.getAuthorIdent().getEmailAddress();
            name = name == null ? commit.getAuthorIdent().getName() : name;
            commitList.getWebModel().addElement(commit.getName() + " | " + name + " | " + commit.getFullMessage());

            Integer commitCount = commitsByAuthor.get(commit.getAuthorIdent());
            if(commitCount == null) {
                commitsByAuthor.put(commit.getAuthorIdent(), new Integer(1));
            } else {
                commitsByAuthor.put(commit.getAuthorIdent(), commitCount + 1);
            }

            String commitMsg = commit.getShortMessage();
            String[] splitMsg = commitMsg.split(" ");
            for (String word : splitMsg) {
                word = word.replace("(", "");
                word = word.replace(")", "");
                word = word.replace(",", "");
                word = word.replace(".", "");
                if (word.equals("")) { continue; }

                if (wordCount.containsKey(word)) {
                    wordCount.put(word, wordCount.get(word) + 1);
                } else {
                    wordCount.put(word, 1);
                }
            }
        }
        String[] headers = {"Word", "Count"};
        String[][] data = new String[wordCount.entrySet().size()][2];
        int index = 0;

        List<Map.Entry<String, Integer>> sortedEntries = wordCount.entrySet().stream().sorted((o1, o2) -> (o2.getValue().compareTo(o1.getValue()))).collect(Collectors.toList());
        for(Map.Entry<String, Integer> wordCountEntry : sortedEntries) {
            data[index][0] = wordCountEntry.getKey();
            data[index][1] = String.valueOf(wordCountEntry.getValue());
            index++;
        }
        wordCountPane.remove(wordCountTable);
        wordCountTable = new WebTable ( data, headers );
        wordCountPane.add(wordCountTable);
        wordCountPane.setViewportView(wordCountTable);

        commitsByAuthorDataset.clear();
        for (Map.Entry<PersonIdent, Integer> entry : commitsByAuthor.entrySet()) {
            String name = entry.getKey().getEmailAddress();
            name = name == null ? entry.getKey().getName() : name;
            commitsByAuthorDataset.setValue(name, entry.getValue());
        }

        comitsByTimeDataset.clear();
        TimeSeries series = new TimeSeries( "Commits by time" );
        for (Map.Entry<PersonIdent, Integer> entry : commitsByAuthor.entrySet()) {
            String name = entry.getKey().getEmailAddress();
            name = name == null ? entry.getKey().getName() : name;

            int hours = entry.getKey().getWhen().getHours();
            String rowKey = getRowKeyForHours(hours);
            try {
                Number currentCommitCounter = comitsByTimeDataset.getValue(rowKey, name);
                comitsByTimeDataset.addValue(currentCommitCounter.intValue()+1, rowKey , name);
            } catch (UnknownKeyException e) {
                comitsByTimeDataset.addValue(1, rowKey , name);
            }
        }



        commitsByTimePanel.repaint();
        commitsByAuthorPanel.repaint();
        commitListPane.repaint();
        wordCountPane.repaint();

        wordCount.entrySet().stream().sorted((o1, o2) -> (o2.getValue().compareTo(o1.getValue()))).collect(Collectors.toList()).
                  stream().forEach(pair -> System.out.println(pair.getValue() + ": " + pair.getKey()));
    }

    private String getRowKeyForHours(int hours) {
        if(hours >= 0 && hours <= 2) {
            return "0 to 2";
        } else if(hours > 2 && hours <= 4) {
            return "2 to 4";
        } else if(hours > 4 && hours <= 12) {
            return "4 to 12";
        } else if(hours > 12 && hours <= 20) {
            return "12 to 20";
        } else return "20 to 24";
    }

    private void showSuccess(String content) {
        final WebNotificationPopup notificationPopup = new WebNotificationPopup();
        notificationPopup.setIcon(NotificationIcon.plus);
        notificationPopup.setDisplayTime(2000);
        notificationPopup.setContent(new WebLabel(content));
        NotificationManager.showNotification(notificationPopup);
    }

    private void showError(String content) {
        final WebNotificationPopup notificationPopup = new WebNotificationPopup();
        notificationPopup.setIcon(NotificationIcon.error);
        notificationPopup.setDisplayTime(2000);
        notificationPopup.setContent(new WebLabel(content));
        NotificationManager.showNotification(notificationPopup);
    }

    public static void main(String[] args) {
        WebLookAndFeel.install();

        GitstatsGui gitstatsGui = new GitstatsGui();
        gitstatsGui.showOpenDirectoryDialog();
    }
}

